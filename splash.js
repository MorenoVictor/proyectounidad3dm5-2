import React, {Component} from 'react'
import {Image, View, StyleSheet, StatusBar, TouchableOpacity, AppRegistry} from 'react-native'

class Splash extends Component{
    render() {
        return (
            <>
                <StatusBar barStyle="dark-content" hidden={false} translucent={ true} backgroundColor='#bdba33'/>
                <View style={styles.container}>
                    <View style={styles.imgContainer}>
                        <TouchableOpacity >
                            <Image source={require('./logopizza.PNG')} style={styles.logo} />
                        </TouchableOpacity>
                    </View>
                </View>
            </>
        )
    }
}

  
export default Splash


const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%'
    },
    imgContainer: {
        flex:1,
        height: '100%',
        justifyContent:'center'
    },
    logo: {
        width: 150,
        height:150
    }
})

import React,{Component} from 'react'
import { Text, View, TouchableOpacity, StyleSheet, StatusBar, ScrollView, Image, Alert } from 'react-native'

class SingleView extends Component{
     state = {
        data: []
    }

    componentDidMount = () => {
        fetch('https://vic.uthds4.info/api/productos', { method: 'GET' })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.setState({
                    data: responseJson
                })
            }) 
            .catch((error) => {
                console.error(error);
            });
            
    }
    alertName=(item)=>{
        alert("Descripción: "+item.descripcion+"."+"\n"+"\n"+"ID: "+item.id);
    }

    render() {
        return (
            <ScrollView>
                <StatusBar StatusBar barStyle="dark-content" hidden={false} backgroundColor='#edab68' translucent={true}/>
                <View style={styles.container}>
                    {
                     this.state.data.map((item, index)=>(
                         <TouchableOpacity key={item.id}  onPress={() => this.alertName(item)}>
                             <Image source={{uri:'https://images3.alphacoders.com/144/thumbbig-144527.jpg'}} style={styles.img}/>
                             <Text style={{fontSize:40, paddingTop:12}}>{item.nombre}</Text>
                                <Text style= {styles.listText}>Precio: $ {item.precio}</Text>
                                <Text style= {styles.listText}>Codigo Barra: {item.codBarra}</Text>
                        </TouchableOpacity>
                             ))
                    }
              
                </View>
            </ScrollView>   
        )
    }
} 

export default SingleView

const styles = StyleSheet.create({
   listText:{
        fontSize:20,
        color : '#000000',
        justifyContent: 'center',
        alignItems: 'center',
       
        
    },

    container:{
        flex: 1,
        backgroundColor:'#f5e4e4',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        height: '100%'
    },

    img:{
        
        width:250,
        height:100,
        alignItems: 'center',
        borderRadius:15 
    },
    

    
});
